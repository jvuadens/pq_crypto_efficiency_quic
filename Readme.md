1. Place every file in the home folder.
2. Run the install.sh script.
3. Once it is done, go in the mininet folder and run mininet_exp.py to start the experiment.
4. Once the experiment is done, you can run plot.py to create graphs from the results.