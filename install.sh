#!/bin/bash

set -ex
ROOT=$(pwd)

apt install python3 \
            astyle \
            cmake \
            gcc \
            ninja-build \
            libssl-dev \
            python3-pytest \
            python3-pytest-xdist \
            unzip \
            xsltproc \
            doxygen \
            graphviz;

apt-get install git-all
apt-get install build-essential
apt-get install libz-dev
apt-get install libevent-dev

#install go
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin


#install and build BoringSSL
mkdir oqs
chmod 777 -R oqs/
cd oqs
git clone https://boringssl.googlesource.com/boringssl
chmod 777 -R boringssl/
cd boringssl
git checkout -f a2278d4d2cabe73f6663e3299ea7808edfa306b9
cmake . &&  make
BORINGSSL=$PWD
cmake -DBUILD_SHARED_LIBS=1 . && make


#install and build liboqs
cd ..
git clone -b main https://github.com/open-quantum-safe/liboqs.git
cd liboqs
mkdir build && cd build
cmake -GNinja ..
ninja



#install and build LSQuic

cd ../..
git clone https://github.com/litespeedtech/lsquic.git
chmod 777 -R lsquic/
mv ../http_client.c lsquic/bin/http_client.c
mv ../http_server.c lsquic/bin/http_server.c
cd lsquic
git submodule init
git submodule update
cmake -DLSQUIC_SHARED_LIB=1 -DBORINGSSL_DIR=$BORINGSSL .
make


#install mininet

cd ../..
git clone git://github.com/mininet/mininet
chmod 777 -R mininet/
cd mininet
git checkout -f -b mininet-2.3.0 2.3.0
util/install.sh -fnv

mkdir plots
mkdir json_results

cd ..
chmod 777 -R oqs/
chmod 777 -R mininet/
mv mininet_exp.py mininet/mininet_exp.py
mv plot.py mininet/plot.py
mv certs oqs/lsquic

