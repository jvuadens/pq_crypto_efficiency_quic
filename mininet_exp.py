from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.clean import Cleanup
import json
import click
import time
import os
import re
import statistics

wd = os.getcwd()
SupportedSigs = [
    "dilithium2", "dilithium2_aes", "dilithium3", "dilithium3_aes", "dilithium5", "dilithium5_aes",
    "falcon512", "falcon1024",
    "rsa"
]
SupportedKex = [
    "p256_frodo640aes", "p256_frodo640shake",
    "p256_bike1l1cpa", "p384_bike1l3cpa",
    "p256_kyber512", "p384_kyber768", "p521_kyber1024",
    "p256_ntru_hps2048509", "p384_ntru_hps2048677", "p521_ntru_hps4096821", "p384_ntru_hrss701",
    "p256_lightsaber", "p384_saber", "p521_firesaber",
    "p256_sidhp434", "p256_sidhp503", "p384_sidhp610", "p521_sidhp751",
    "p256_sikep434", "p256_sikep503", "p384_sikep610", "p521_sikep751",
    "p256_bike1l1fo", "p384_bike1l3fo",
    "p256_kyber90s512", "p384_kyber90s768", "p521_kyber90s1024",
    "p256_hqc128", "p384_hqc192",
    "p256_ntrulpr653", "p384_ntrulpr761", "p384_ntrulpr857",
    "p256_sntrup653", "p384_sntrup761", "p384_sntrup857"
]

CipherText_size = {
    "p256_frodo640aes": 9720, "p256_frodo640shake": 9720,
    "p256_bike1l1cpa": 2542, "p384_bike1l3cpa": 4964,
    "p256_kyber512": 768, "p384_kyber768": 1088, "p521_kyber1024": 1568,
    "p256_ntru_hps2048509": 699, "p384_ntru_hps2048677": 930, "p521_ntru_hps4096821": 1230, "p384_ntru_hrss701": 1138,
    "p256_lightsaber": 736, "p384_saber": 1088, "p521_firesaber": 1472,
    "p256_sidhp434": 330, "p256_sidhp503": 378, "p384_sidhp610": 462, "p521_sidhp751": 564,
    "p256_sikep434": 346, "p256_sikep503": 402, "p384_sikep610": 486, "p521_sikep751": 596,
    "p256_bike1l1fo": 2946, "p384_bike1l3fo": 6206,
    "p256_kyber90s512": 768, "p384_kyber90s768": 1088, "p521_kyber90s1024": 1568,
    "p256_hqc128": 4481, "p384_hqc192": 9026,
    "p256_ntrulpr653": 1025, "p384_ntrulpr761": 1167, "p384_ntrulpr857": 1312,
    "p256_sntrup653": 897, "p384_sntrup761": 1039, "p384_sntrup857": 1184
}

PublicKey_size = {
    "p256_frodo640aes": 9616, "p256_frodo640shake": 9616,
    "p256_bike1l1cpa": 2542, "p384_bike1l3cpa": 4964,
    "p256_kyber512": 800, "p384_kyber768": 1184, "p521_kyber1024": 1568,
    "p256_ntru_hps2048509": 699, "p384_ntru_hps2048677": 930, "p521_ntru_hps4096821": 1230, "p384_ntru_hrss701": 1138,
    "p256_lightsaber": 672, "p384_saber": 992, "p521_firesaber": 1312,
    "p256_sidhp434": 330, "p256_sidhp503": 378, "p384_sidhp610": 462, "p521_sidhp751": 564,
    "p256_sikep434": 330, "p256_sikep503": 378, "p384_sikep610": 462, "p521_sikep751": 564,
    "p256_bike1l1fo": 2946, "p384_bike1l3fo": 6206,
    "p256_kyber90s512": 800, "p384_kyber90s768": 1184, "p521_kyber90s1024": 1568,
    "p256_hqc128": 2249, "p384_hqc192": 4522,
    "p256_ntrulpr653": 897, "p384_ntrulpr761": 1039, "p384_ntrulpr857": 1184,
    "p256_sntrup653": 994, "p384_sntrup761": 1158, "p384_sntrup857": 1322
}
not_working_kex = []
not_working_sig = []


class DumbbellTopo(Topo):
    def build(self, bw=8, delay="10ms", loss=0, nbr_nodes=1):
        clients = []
        switch1 = self.addSwitch('switch1')
        switch2 = self.addSwitch('switch2')
        for i in range(nbr_nodes):
            appClient = self.addHost('aClient' + str(i))
            clients.append(appClient)
            crossClient = self.addHost('cClient' + str(i))
            self.addLink(appClient, switch1)
            self.addLink(crossClient, switch1)
        appServer = self.addHost('aServer')
        crossServer = self.addHost('cServer')
        self.addLink(appServer, switch2)
        self.addLink(crossServer, switch2)
        print("loss :")
        print(loss)
        self.addLink(switch1, switch2, bw=bw, delay=delay, loss=loss, max_queue_size=14)


def parse_return(ret, tmp, client_id):
    """
    Format :
    Collecting Connection statistics for 5 seconds
    *******

    Line 27 : time to connect 
    Line 28 : time for request
    Line 29 : time to 1st byte
    Line 37 : Total bytes IN
    line 38 : # packets IN
    line 49 : Total bytes OUT
    line 50 : # packets OUT
    """
    lines = ret.split("\n")
    connect = lines.index('overall statistics as calculated by ./http_client:\r') + 1
    # nb_requests = lines[connect].split(" ")[4][:-1]
    # if nb_requests != nbr_req and phase:
    #    not_working_kex.append(kex)
    #    return f"COULDN'T RETRIEVE ANY INFO"
    # if nb_requests != nbr_req and not phase:
    #    not_working_sig.append(sig)
    #    return f"COULDN'T RETRIEVE ANY INFO"
    # secs_connect
    tmp[0][client_id] = float(lines[connect].split(" ")[6])
    # secs_request
    tmp[1][client_id] = float(lines[connect + 1].split(" ")[6])
    # secs_firstbyte
    tmp[2][client_id] = float(lines[connect + 2].split(" ")[7])

    return tmp


def parse_json(tmp, kex, sig, phase, nbr_nodes, loss):
    results = []
    print("\ntmp : ", tmp)
    final_tmp = compute_mean(tmp)
    print("\nfinal_tmp : ", final_tmp)
    ret = {
        "kex": kex,
        "sig": sig,
        "ct_size": CipherText_size[kex],
        "pk_size": PublicKey_size[kex],
        "nbr_nodes": nbr_nodes,
        "loss": loss,
        "secs_connect_min": final_tmp[0],
        "secs_connect_max": final_tmp[1],
        "secs_connect_mean": final_tmp[2],
        "secs_request_min": final_tmp[3],
        "secs_request_max": final_tmp[4],
        "secs_request_mean": final_tmp[5],
        "secs_firstbyte_min": final_tmp[6],
        "secs_firstbyte_max": final_tmp[7],
        "secs_firstbyte_mean": final_tmp[8],
    }
    results.append(ret)
    to_json(results, phase, loss)
    return ret


def compute_mean(tmp):
    res = []
    for i in range(len(tmp)):
    	tab = []
    	for j in range(len(tmp[i])):
    		a = tmp[i][j]
    		if a > 0.0:
    			tab.append(a)
    	tab.sort()
    	res.append(tab)
    final_tmp = [None] * (3*len(res))

    for i in range(len(res)):
    	idx = i * 3
    	if not res[i]:
    		final_tmp[idx] = 0
    		final_tmp[idx+1] = 0
    		final_tmp[idx+2] = 0
    	else :
    		#fill min
    		final_tmp[idx] = min(res[i])
    		#fill max
    		final_tmp[idx+1] = max(res[i])
    		#fill median
    		final_tmp[idx + 2] = statistics.median(res[i])	
    return final_tmp


def to_json(results, phase, loss):
    if phase:
        filename = f"json_results/results_kex_{loss}.json"
    else:
        filename = f"json_results/results_sig_{loss}.json"
    with open(filename, "r") as infile:
        exp = json.load(infile)
    results = results + exp
    with open(filename, 'w+') as outfile:
        json.dump(results, outfile)


def empty_json(loss):
    data = []
    filename1 = f"json_results/results_kex_{loss}.json"
    filename2 = f"json_results/results_sig_{loss}.json"
    with open(filename1, 'w') as outfile:
        json.dump(data, outfile)
    with open(filename2, 'w') as outfile:
        json.dump(data, outfile)


def start_client(addr, kem):
    command = f"./http_client -H {addr} -s {addr} -p / -x {kem} -t"
    print("\nLaunching client with ")
    print(" > " + command)
    return command


def start_server(addr, sig):
    command = f"cd ../oqs/lsquic/bin && ./http_server -c {addr},../certs/{sig}/key_crt.pem,../certs/{sig}/key_srv.pem -s 0.0.0.0:4433 &"
    print("\nLaunching server with ")
    print(" > " + command)
    return command


def signature_switch(port, kex, sig, loss, nbr_nodes):
    for i in SupportedSigs:
        a = Cleanup()
        a.cleanup()
        dumbbell = DumbbellTopo(loss=loss, nbr_nodes=nbr_nodes)
        network = Mininet(topo=dumbbell, link=TCLink)
        network.start()
        appServer = network.get('aServer')
        addr = appServer.IP() + ":" + port
        appServer.cmd(start_server(addr, i))
        time.sleep(1)  # Server might need some time to start
        clients = []
        tmp = [[0 for x in range(nbr_nodes)] for y in range(3)]
        for j in range(nbr_nodes):
            appClient = network.get('aClient' + str(j))
            appClient.cmd("cd ../oqs/lsquic/bin")
            ret = appClient.cmd(start_client(addr, kex))
            tmp = parse_return(ret, tmp, j)
        parse_json(tmp, kex, i, False, nbr_nodes, loss)
        network.stop()


def kem_switch(port, kex, sig, loss, nbr_nodes):
    for i in SupportedKex:
        a = Cleanup()
        a.cleanup()
        dumbbell = DumbbellTopo(loss=loss, nbr_nodes=nbr_nodes)
        network = Mininet(topo=dumbbell, link=TCLink)
        network.start()
        appServer = network.get('aServer')
        addr = appServer.IP() + ":" + port
        appServer.cmd(start_server(addr, sig))
        time.sleep(1)  # Server might need some time to start
        clients = []
        tmp = [[0 for x in range(nbr_nodes)] for y in range(3)] 
        for j in range(nbr_nodes):
            appClient = network.get('aClient' + str(j))
            appClient.cmd("cd ../oqs/lsquic/bin")
            ret = appClient.cmd(start_client(addr, i))
            tmp = parse_return(ret, tmp, j)
        parse_json(tmp, i, sig, True, nbr_nodes, loss)
        network.stop()


@click.command()
@click.option('--tls_port', default="4433", help="The port of the TLS server")
@click.option('--sig', default="dilithium5", help="Use this to force a signature algorithm.")
@click.option('--kex', default="p256_sikep434", help="Use this to force a key exchange algorithm.")
@click.option('--nbr_nodes', default=10, help="Number of Nodes")
def main(tls_port, sig, kex, nbr_nodes):
    if sig not in SupportedSigs:
        print("Signature algorithm not supported.")
    elif kex not in SupportedKex:
        print("Key Exchange algorithm not supported.")
    elif nbr_nodes <1:
        print("Invalid number of clients")
    elif re.search("[0-9]+$", tls_port) is None:
        print("Non valid port value.")
    else:
        for i in range(0, 22, 2):
            empty_json(i)
            print("\nKEM SWITCH, LOSS : ", i)
            kem_switch(tls_port, kex, sig, i, nbr_nodes)
            print("\nSIG SWITCH, LOSS : ", i)
            signature_switch(tls_port, kex, sig, i, nbr_nodes)
            print("\nNot working Kex : ")
            print(not_working_kex)
            print("\nNot working Sig : ")
            print(not_working_sig)


if __name__ == '__main__':
    main()
