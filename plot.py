import json
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

print("Loading Experiment...")
#mininet_exp.main()

# KEM PLOT -------------------------------------------------------------

for i in range(0,12,2):
	filename = f"json_results/results_kex_{i}.json"
	with open(filename,"r") as infile:
		data = json.load(infile)

	mean_connect_time = [float(i['secs_connect_mean']) for i in data]

	kex = [i['kex'] for i in data]
	sig = [i['sig'] for i in data]

	plt.bar(kex,mean_connect_time)
	plt.xticks(kex, kex, rotation='vertical')
	plt.xlabel("Key Exchange Mechanism")
	plt.ylabel("Mean time per connection (ms)")
	title = f"Connection time of different KEM with dilithium5 and {i}% loss"
	plt.title(title)
	plt.grid()
	save = f"plots/connection_timings_plot_KEM_{i}.png"
	plt.savefig(save,bbox_inches='tight')
	plt.clf()
	# plt.show()

# SIG PLOT -------------------------------------------------------------

	filename = f"json_results/results_sig_{i}.json"
	with open(filename,"r") as infile:
		data = json.load(infile)

	mean_connect_time = [float(i['secs_connect_mean']) for i in data]

	kex = [i['kex'] for i in data]
	sig = [i['sig'] for i in data]

	plt.bar(sig,mean_connect_time)
	plt.xticks(sig, sig, rotation='vertical')
	plt.xlabel("SIG")
	plt.ylabel("Mean time per connection (ms)")
	title = f"Connection time of different SIG with p256_sikep434 and {i}% loss"
	plt.title(title)
	plt.grid()
	save = f"plots/connection_timings_plot_SIG_{i}.png"
	plt.savefig(save,bbox_inches='tight')
	plt.clf()
	# plt.show()
	

# LOSS KEM PLOT -------------------------------------------------------------

def retrieve_data_kex(name, tab):
	for i in tab:
		if i['kex'] == name:
			return float(i['secs_connect_mean'])
			

timings = []
kex = ["p256_kyber512","p256_bike1l1cpa","p256_lightsaber","p256_sikep434","p256_ntrulpr653"]
loss = [0, 2, 4, 8]
for i in loss:
	filename = f"json_results/results_kex_{i}.json"
	with open(filename,"r") as infile:
		data = json.load(infile)

	timing =[]
	for j in kex:
		timing.append(retrieve_data_kex(j,data))
	timings.append(timing)

res = np.array(timings)
res = res.T.tolist()

for i in range(len(kex)):
	x = loss
	y = res[i]
	plt.plot(x,y,label = kex[i])

plt.xlabel("Loss Rate")
plt.ylabel("Mean time per connection (ms)")
title = f"Evoluation of connection time with loss of different KEM with dilithium5"
plt.title(title)
plt.grid()
plt.legend()
save = f"plots/connection_timings_plot_loss_KEM.png"
plt.savefig(save,bbox_inches='tight')
plt.clf()
	

# LOSS SIG PLOT -------------------------------------------------------------

def retrieve_data_sig(name, tab):
	for i in tab:
		if i['sig'] == name:
			return float(i['secs_connect_mean'])
			

timings = []
sig = ["falcon512","dilithium2","dilithium3","dilithium5"]

for i in loss:
	filename = f"json_results/results_sig_{i}.json"
	with open(filename,"r") as infile:
		data = json.load(infile)

	timing =[]
	for j in sig:
		timing.append(retrieve_data_sig(j,data))
	timings.append(timing)

res = np.array(timings)
res = res.T.tolist()

for i in range(len(sig)):
	x = loss
	y = res[i]
	plt.plot(x,y,label = sig[i])

plt.xlabel("Loss Rate")
plt.ylabel("Mean time per connection (ms)")
title = f"Evoluation of connection time with loss of different SIG with p256_sikep434"
plt.title(title)
plt.legend()
plt.grid()
save = f"plots/connection_timings_plot_loss_SIG.png"
plt.savefig(save,bbox_inches='tight')
plt.clf()


# SIZE KEM PLOT -------------------------------------------------------------

def retrieve_kem_size(name, tab):
	for i in tab:
		if i['kex'] == name:
			return int(i['ct_size'])

filename = f"json_results/results_kex_0.json"
with open(filename,"r") as infile:
	data = json.load(infile)

timing_0 =[]
kem_size = []
for i in kex:
	timing_0.append(retrieve_data_kex(i,data))
	kem_size.append(retrieve_kem_size(i,data))

filename = f"json_results/results_kex_8.json"
with open(filename,"r") as infile:
	data = json.load(infile)

timing_8 =[]
kem_size = []
for i in kex:
	timing_8.append(retrieve_data_kex(i,data))
	kem_size.append(retrieve_kem_size(i,data))


plt.scatter(kem_size, timing_0)
for i,txt in enumerate(kex):
	plt.annotate(txt, (kem_size[i], timing_0[i]))

plt.xlabel("Ciphertext size (bytes)")
plt.ylabel("Mean time per connection (ms)")
title = f"Evoluation of connection time with ciphertext size for low loss rate"
plt.title(title)
plt.grid()
save = f"plots/connection_timings_plot_low_loss_keyKEM.png"
plt.savefig(save,bbox_inches='tight')
plt.clf()

plt.scatter(kem_size, timing_8)
for i,txt in enumerate(kex):
	plt.annotate(txt, (kem_size[i], timing_8[i]))

plt.xlabel("Ciphertext size (bytes)")
plt.ylabel("Mean time per connection (ms)")
title = f"Evoluation of connection time with ciphertext size for high loss rate"
plt.title(title)
plt.grid()
save = f"plots/connection_timings_plot_high_loss_keyKEM.png"
plt.savefig(save,bbox_inches='tight')
plt.clf()




# SIZE SIG PLOT -------------------------------------------------------------
sig_size = [690, 2420, 3293, 4595]


filename = f"json_results/results_sig_0.json"
with open(filename,"r") as infile:
	data = json.load(infile)

timing_0 =[]
for i in sig:
	timing_0.append(retrieve_data_sig(i,data))

filename = f"json_results/results_sig_8.json"
with open(filename,"r") as infile:
	data = json.load(infile)

timing_8 =[]
for i in sig:
	timing_8.append(retrieve_data_sig(i,data))


plt.scatter(sig_size, timing_0)
for i,txt in enumerate(sig):
	plt.annotate(txt, (sig_size[i], timing_0[i]))

plt.xlabel("Signature size (bytes)")
plt.ylabel("Mean time per connection (ms)")
title = f"Evolution of connection time with signature size for low loss rate"
plt.title(title)
plt.grid()
save = f"plots/connection_timings_plot_low_loss_keySIG.png"
plt.savefig(save,bbox_inches='tight')
plt.clf()

plt.scatter(sig_size, timing_8)
for i,txt in enumerate(sig):
	plt.annotate(txt, (sig_size[i], timing_8[i]))

plt.xlabel("Signature size (bytes)")
plt.ylabel("Mean time per connection (ms)")
title = f"Evolution of connection time with signature size for high loss rate"
plt.title(title)
plt.grid()
save = f"plots/connection_timings_plot_high_loss_keySIG.png"
plt.savefig(save,bbox_inches='tight')
plt.clf()















